$LOGFILE = "c:\logs\plugin-windows-owncloud-client.log"

Function Write-Log([String] $logText) {
  '{0:u}: {1}' -f (Get-Date), $logText | Out-File $LOGFILE -Append
}

Function Main {

  Write-Log "Start windows-owncloud-client"
 
  try {

    
    #install anaconda
    If(Test-Path -Path "$env:ProgramData\Chocolatey") {
      choco feature enable -n allowGlobalConfirmation
      choco install owncloud-client --no-progress >> $LOGFILE
    }
    Else {
      Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
      choco feature enable -n allowGlobalConfirmation
      choco install owncloud-client --no-progress >> $LOGFILE
    }
  }
  catch {
      Write-Log "$_"
      Throw $_
  }
  
  Write-Log "End windows-owncloud-client"
 
}

Main    
